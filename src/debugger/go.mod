module src/debugger

go 1.16

replace src/numbers => ./../numbers

replace src/guessers => ./../guessers

replace src/taskworker => ./../taskworker

replace src/api => ./../api

replace src/pkg => ./../pkg

require (
	src/api v0.0.0-00010101000000-000000000000
	src/guessers v0.0.0-00010101000000-000000000000
	src/numbers v0.0.0-00010101000000-000000000000
	src/taskworker v0.0.0-00010101000000-000000000000
)
