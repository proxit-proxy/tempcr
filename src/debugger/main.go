package main

import (
	"src/api/api"
	"src/guessers/guessers"
	"src/numbers/numbers"
	"src/taskworker/taskworker"
)

func main() {
	go numbers.RealMain()
	go api.RealMain()
	go guessers.RealMain()
	go taskworker.RealMain()
	select {}
}
