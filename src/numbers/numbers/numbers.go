package numbers

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	_ "go.mongodb.org/mongo-driver/mongo/options"
	_ "go.mongodb.org/mongo-driver/mongo/readconcern"
	_ "go.mongodb.org/mongo-driver/mongo/writeconcern"
	"google.golang.org/grpc"
	"log"
	"net"
	numberspb "src/numbers/pb"
	"src/pkg/asynctasks/client"
	"src/pkg/consul"
	"src/pkg/mongowrapper"
	"strconv"
	"time"
)

type NumberDb struct {
	Id            primitive.ObjectID `bson:"_id"`
	Number        int32              `bson:"number"`
	Guessers      []string           `bson:"guessers"`
	Datetime      []int64            `bson:"datetime"`
	AttemptNumber []int32            `bson:"attempt_number"`
	IsActive      bool               `bson:"is_active"`
}

type server struct {
	numberspb.UnimplementedNumberServiceServer
}

const grpcport string = "50053"

func RealMain() int {
	consul.RegisterServiceConsul("numbers", grpcport)
	fmt.Println("Numbers: Numbers server")

	lis, err := net.Listen("tcp", "0.0.0.0:"+grpcport)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	numberspb.RegisterNumberServiceServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	return 1
}

func (*server) AddNumber(ctx context.Context, req *numberspb.AddNumberRequest) (*numberspb.AddNumberResponse, error) {
	if req.Number == 0 {
		return &numberspb.AddNumberResponse{Ok: false}, nil
	}
	fmt.Printf("Numbers: Add Number %v\n", req)

	numbersCollection := mongowrapper.NewMongoConnector().GetDB().Collection("numbers")

	var res NumberDb
	numbersCollection.FindOne(ctx, bson.D{{"number", req.Number}}).Decode(&res)

	fmt.Println("Numbers: Received GuessNumber RPC for", req)

	if res.Number != 0 {
		fmt.Println("Numbers: Found", res)
		if res.IsActive {
			return &numberspb.AddNumberResponse{Ok: false}, nil
		} else {
			updateres, err := numbersCollection.UpdateOne(ctx, bson.D{{"_id", res.Id}}, bson.D{{"$set", bson.M{"is_active": true}}})
			if err != nil {
				if mongo.IsNetworkError(err) || mongo.IsTimeout(err) {
					fmt.Printf("Numbers: Add Number mongo error %v\nRecreating client\n", err)
				}
				fmt.Printf("Numbers: Add Number mongo error %v\n", err)
				return &numberspb.AddNumberResponse{Ok: false}, nil
			}
			fmt.Printf("Numbers: Add Number mongo result %v\n", updateres)
			return &numberspb.AddNumberResponse{Ok: true}, nil
		}
	}

	doc := &NumberDb{
		Id:            primitive.NewObjectID(),
		Number:        req.Number,
		Guessers:      []string{},
		Datetime:      []int64{},
		AttemptNumber: []int32{},
		IsActive:      true,
	}

	addNumberRes, err := numbersCollection.InsertOne(context.Background(), doc)
	if err != nil {
		if mongo.IsNetworkError(err) || mongo.IsTimeout(err) {
			fmt.Printf("Numbers: Add Number mongo error %v\nRecreating client\n", err)
		}
		fmt.Printf("Numbers: Add Number mongo error %v\n", err)
		return &numberspb.AddNumberResponse{Ok: false}, nil
	}
	fmt.Printf("Numbers: Add Number mongo result %v\n", addNumberRes)
	return &numberspb.AddNumberResponse{Ok: true}, nil
}

func (*server) RemoveNumber(ctx context.Context, req *numberspb.RemoveNumberRequest) (*numberspb.RemoveNumberResponse, error) {
	fmt.Printf("Numbers: Remove Number %v\n", req)

	numbersCollection := mongowrapper.NewMongoConnector().GetDB().Collection("numbers")

	if req.ClearDbRecord {
		deleteRes, err := numbersCollection.DeleteOne(context.Background(), bson.D{{"number", req.Number}})
		if err != nil {
			return &numberspb.RemoveNumberResponse{Ok: false}, nil
		}
		if deleteRes.DeletedCount < 1 {
			fmt.Println("Numbers: No record to remove,", req.Number, "doesnt exist on db")
		}
	} else {
		updateres, err := numbersCollection.UpdateOne(ctx, bson.D{{"number", req.Number}}, bson.D{{"$set", bson.M{"is_active": false}}})
		if err != nil {
			if mongo.IsNetworkError(err) || mongo.IsTimeout(err) {
				fmt.Printf("Numbers: Remove Number mongo error %v\nRecreating client\n", err)
			}
			fmt.Printf("Numbers: Remove Number mongo error %v\n", err)
			return &numberspb.RemoveNumberResponse{Ok: false}, nil
		}
		if updateres.UpsertedCount < 1 {
			fmt.Println("Numbers: No record to remove,", req.Number, "doesnt exist on db")
		}
		fmt.Printf("Numbers: Remove Number mongo result %v\n", updateres)
	}
	return &numberspb.RemoveNumberResponse{Ok: true}, nil
}

func (*server) GuessNumber(ctx context.Context, req *numberspb.GuessNumberRequest) (*numberspb.GuessNumberResponse, error) {

	numbersCollection := mongowrapper.NewMongoConnector().GetDB().Collection("numbers")

	var res NumberDb
	dbRes := numbersCollection.FindOne(ctx, bson.D{{"number", req.GuessedNumber}})
	dbRes.Decode(&res)
	if res.Id != primitive.NilObjectID && res.IsActive {
		// ++
		collections := []string{"numbers", "guessers"}
		filters := make(map[string]bson.D)

		numbersfilter := bson.D{{"number", req.GuessedNumber}}
		filters["numbers"] = numbersfilter
		guessersfilter := bson.D{{"guesser_id", req.GuesserId}}
		filters["guessers"] = guessersfilter

		changes := make(map[string]bson.D)
		numberschanges := bson.D{{
			"$push", bson.M{"guessers": req.GuesserId, "attempt_number": req.GuesserAttempt, "datetime": time.Now().Unix()},
		}}
		changes["numbers"] = numberschanges
		guesserschanges := bson.D{{
			"$push", bson.M{"good_guesses": strconv.Itoa(int(req.GuessedNumber)), "attempt_number": req.GuesserAttempt, "datetime": time.Now().Unix()},
		}}
		changes["guessers"] = guesserschanges

		go SearchForPrime(int(req.GuessedNumber), req.GuesserId)
		if mongowrapper.NewMongoConnector().RunTransaction(collections, filters, changes) {
			// Success
			fmt.Println("Numbers: Success to add to db with transaction")
		} else {
			fmt.Println("Numbers: Failure to add to db with transaction")
		}
		return &numberspb.GuessNumberResponse{Correct: true}, nil
	}
	fmt.Println("Numbers: incorrect guess of", req.GuessedNumber, "by", req.GuesserId, "attempt #", req.GuesserAttempt)
	return &numberspb.GuessNumberResponse{Correct: false}, nil
}

func SearchForPrime(startFrom int, guesserId string) {
	_, err := client.SendFindClosestPrime(startFrom, guesserId)
	if err != nil {
		fmt.Println("Failed finding prime for (number, who):", startFrom, guesserId)
	}
}
