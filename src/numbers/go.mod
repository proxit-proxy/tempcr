module src/numbers

go 1.16

replace src/numbers/ => ./numbers/

replace src/pkg => ./../pkg

require (
	go.mongodb.org/mongo-driver v1.5.4
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
	src/pkg v0.0.0-00010101000000-000000000000
)
