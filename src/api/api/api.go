package api

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"src/guessers/guessers"
	guesserpb "src/guessers/pb"
	"src/numbers/numbers"
	numberspb "src/numbers/pb"
	"src/pkg/asynctasks/tasks"
	"src/pkg/consul"
	"src/pkg/mongowrapper"
	"src/pkg/redis"
	"strings"
	"time"
)

type server struct {
	guesserpb.UnimplementedGatewayServer
	client numberspb.NumberServiceClient
	grpcnumberconnection *grpc.ClientConn
	uses	int
}

// Channels definition
var createGuesserChannel chan AddGuesserRequest
var removeGuesserChannel chan RemoveGuesserRequest

var addNumberChannel chan AddNumberRequest
var removeNumberChannel chan RemoveNumberRequest

type GuesserStream struct {
	request       *numberspb.GuessNumberRequest
	guesserStream guesserpb.Gateway_GuessServer
}

const grpcport string = "50051"
const restport string = "8099"

func RealMain() int {
	consul.RegisterServiceConsul("api", grpcport)
	killChannel := make(chan bool)
	http.HandleFunc("/query_primes", QueryPrimes)
	// query_guesser	- Get guesser with the correct guesses and when they were made along with its definition (by  guesser_id)
	http.HandleFunc("/query_guesser", QueryGuesser)
	// query_server		- Get a server details - numbers, guessers and timestamp
	http.HandleFunc("/query_number", QueryNumber)
	GuesserGatewayServer()

	NumbersClientHandler()
	StartGuesserServiceAPI()
	fmt.Println("API: Waiting on kill")
	<-killChannel
	fmt.Println("API KILLED")
	return 1
}

func NumbersClientHandler() {
	addNumberChannel = make(chan AddNumberRequest)
	removeNumberChannel = make(chan RemoveNumberRequest)

	go func(addNumberChannel chan AddNumberRequest, removeGuesserChannel chan RemoveNumberRequest) {
		var client numberspb.NumberServiceClient
		var grpcnumberconnection *grpc.ClientConn
		numberscalls := 0
		defer grpcnumberconnection.Close()
		for {
			select {
			case request := <-addNumberChannel:
				numberscalls++
				if numberscalls > 10 {
					fmt.Println("Called number ten times, reconnecting")
					grpcnumberconnection.Close()
					numberscalls = 0
				}
				if grpcnumberconnection == nil || (grpcnumberconnection.GetState() != connectivity.Ready && grpcnumberconnection.GetState() != connectivity.Idle) {
					fmt.Println("Calling reconnect")
					client, grpcnumberconnection = CallNumberService()
				}

				response, err := client.AddNumber(context.Background(),
					&numberspb.AddNumberRequest{
						Number: request.Number,
					})

				var apiresponse *AddNumberResponse
				if err != nil {
					apiresponse = &AddNumberResponse{
						Result: "failure",
						Error:  err.Error(),
					}
				} else if !response.Ok {
					apiresponse = &AddNumberResponse{
						Result: "failure",
						Error:  "already exists and active",
					}
				} else {
					fmt.Printf("API: Received response %v\n", response.Ok)
					apiresponse = &AddNumberResponse{
						Result: "success",
					}
				}
				request.ResponseChannel <- apiresponse
			case request := <-removeNumberChannel:
				numberscalls++
				if numberscalls > 10 {
					fmt.Println("Called number ten times, reconnecting")
					grpcnumberconnection.Close()
					numberscalls = 0
				}
				if grpcnumberconnection == nil || (grpcnumberconnection.GetState() != connectivity.Ready && grpcnumberconnection.GetState() != connectivity.Idle) {
					fmt.Println("Calling reconnect")
					client, grpcnumberconnection = CallNumberService()
				}

				response, err := client.RemoveNumber(context.Background(),
					&numberspb.RemoveNumberRequest{
						Number:        request.Number,
						ClearDbRecord: request.ClearDbRecord,
					})

				var apiresponse *RemoveNumberResponse
				if err != nil {
					fmt.Printf("API: Failed removing number, %v\n", err)
					apiresponse = &RemoveNumberResponse{
						Result: "failure",
						Error:  err.Error(),
					}
				} else {
					fmt.Printf("API: Received response %v\n", response)
					apiresponse = &RemoveNumberResponse{
						Result: "success",
					}
				}
				request.ResponseChannel <- apiresponse
			default:
				time.Sleep(1 * time.Second)
			}

		}
	}(addNumberChannel, removeNumberChannel)
	// add_num			- Adds a number to the numbered servers (to the appropriate server)
	http.HandleFunc("/add_number", AddNumber)
	// remove_num		- The opposite. Optional: remove mongo data.
	http.HandleFunc("/remove_number", RemoveNumber)
	// query_primes 	- Gets the primes discovered with their appropriate data.  Optional:  get the most recent updated / first discovered etc.
}

func CallNumberService() (numberspb.NumberServiceClient, *grpc.ClientConn) {
	numberscc := consul.CallService("numbers")
	return numberspb.NewNumberServiceClient(numberscc), numberscc
}

func StartGuesserServiceAPI() {
	// Wait for API server to go online
	go func() {
		createGuesserChannel = make(chan AddGuesserRequest)
		removeGuesserChannel = make(chan RemoveGuesserRequest)
		go func(createGuesserChannel chan AddGuesserRequest, removeGuesserChannel chan RemoveGuesserRequest) {
			for {
				select {
				case request := <-createGuesserChannel:
					fmt.Printf("API: create Guesser %v\n", request)
					client := consul.CallService("guessers")
					guesserServiceClient := guesserpb.NewGuesserServiceClient(client)
					response, err := guesserServiceClient.CreateGuesser(context.Background(),
						&guesserpb.CreateGuesserRequest{
							BeginAt:     request.BeginAt,
							IncrementBy: request.IncrementBy,
							SleepFor:    request.SleepMs,
						})
					client.Close()
					var apiresponse *AddGuesserResponse
					if err != nil {
						fmt.Printf("API: Failed Creating Guesser, %v\n", err)
						apiresponse = &AddGuesserResponse{
							Result: "failure",
							Error:  err.Error(),
						}
					} else {
						fmt.Printf("API: Received response %v\n", response)
						apiresponse = &AddGuesserResponse{
							Result:    "success",
							GuesserId: response.GuesserId,
						}
					}
					request.ResponseChannel <- apiresponse
				case request := <-removeGuesserChannel:
					// Send to all guesser servers
					addressestried := make(map[string]bool)
					var apiresponse *RemoveGuesserResponse

					for {
						connectiontotry, address := consul.CallNextServiceInstance(addressestried, "guessers")
						// We tried all available services
						if connectiontotry == nil {
							apiresponse = &RemoveGuesserResponse{
								Result: "failure",
								Error:  "Not found on any of the guessers",
							}
							break
						}
						addressestried[address] = true
						guesserServiceClient := guesserpb.NewGuesserServiceClient(connectiontotry)
						response, err := guesserServiceClient.RemoveGuesser(context.Background(),
							&guesserpb.RemoveGuesserRequest{
								GuesserId:     request.GuesserId,
								ClearDbRecord: request.ClearDb,
							})
						connectiontotry.Close()
						if err != nil {
							fmt.Printf("API: Failed Removing Guesser, %v\n", err)

							apiresponse = &RemoveGuesserResponse{
								Result: "failure",
								Error:  err.Error(),
							}
						} else {
							fmt.Printf("API: Received response %v\n", response)
							if !response.Ok {
								fmt.Printf("API: Guesser isn't on this client\n", response)
								continue
							}
							apiresponse = &RemoveGuesserResponse{
								Result: "success",
							}
							break
						}
					}
					request.ResponseChannel <- apiresponse
				}
			}
		}(createGuesserChannel, removeGuesserChannel)

		// add_guesser 		- Adds a guesser with beginAt, Inc, sleep. returns guesser_id
		http.HandleFunc("/add_guesser", AddGuesser)
		// remove_guesser	- The opposite, based on id. Optional: remove mongo data.
		http.HandleFunc("/remove_guesser", RemoveGuesser)
	}()
}

func GuesserGatewayServer() {
	fmt.Printf("API: GuesserGatewayServer\n")
	lis, err := net.Listen("tcp", "0.0.0.0:"+grpcport)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	guesserpb.RegisterGatewayServer(s, &server{})

	go func() {
		fmt.Println("API: GuesserGatewayServer: Listening to", grpcport)
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	// Listen to REST API
	go func() {
		fmt.Println("API: REST: Listening on", restport)
		listenErr := http.ListenAndServe(":" + restport, nil)
		fmt.Printf("API: Exiting, err: %v\n", listenErr)
	}()
}

func (s *server) Guess(guesserStream guesserpb.Gateway_GuessServer) error {
	fmt.Println("API: Received Guess RPC streaming connection")

	for {
		req, err := guesserStream.Recv()
		if err == io.EOF {
			fmt.Println("API: Guess streaming received EOF")
			return nil
		}
		if err != nil {
			log.Fatalf("Error while reading guesser stream: %v", err)
			return err
		}
		fmt.Printf("API: Received guess request for %v\n", req)
		attempt := redis.NewRedisAttemptSession().GetAndInc(&redis.AttemptSession{
			GuesserId:       req.GuesserId,
			GuesserServerId: req.ServerId,
		})

		if s.uses > 10 {
			s.uses = 0
			fmt.Println("Called number ten times, reconnecting")
			s.grpcnumberconnection.Close()
		}
		if s.grpcnumberconnection == nil || (s.grpcnumberconnection.GetState() != connectivity.Ready && s.grpcnumberconnection.GetState() != connectivity.Idle) {
			fmt.Println("Calling reconnect")
			s.client, s.grpcnumberconnection = CallNumberService()
		}
		s.uses++

		guessresponse, err := s.client.GuessNumber(context.Background(), &numberspb.GuessNumberRequest{
			GuesserId:      req.GuesserId,
			GuessedNumber:  req.GuessedNumber,
			GuesserAttempt: attempt,
		})
		fmt.Println("API: Returned from guess with error:", err, "guess response", guessresponse)
		if err != nil {
			guesserStream.Send(&guesserpb.GuessResponse{
				SuccessfulGuess: false,
				Ok:              false,
				GuesserId:       req.GuesserId,
			})
			continue
		}

		guesserStream.Send(&guesserpb.GuessResponse{
			SuccessfulGuess: guessresponse.Correct,
			Ok:              true,
			GuesserId:       req.GuesserId,
		})
	}
}

type QueryNumberRequest struct {
	Number int32
}

type QueryNumberExtraDetails struct {
	Number         int32 `json:"number"`
	IsActive       bool  `json:"is_active"`
	FoundByFoundAt map[string]time.Time
}

type QueryNumberResponse struct {
	Result       string                   `json:"result"`
	Error        string                   `json:"error,omitempty"`
	ExtraDetails *QueryNumberExtraDetails `json:"extra_details,omitempty"`
}

func QueryNumber(writer http.ResponseWriter, request *http.Request) {
	dec := UnpackBody(request)

	var clientreq QueryNumberRequest
	for {
		if err := dec.Decode(&clientreq); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("API: {\n\tNumber:\t%d\n}\n", clientreq.Number)
	}
	// Query mongo for number
	var res numbers.NumberDb
	numbersCollection := mongowrapper.NewMongoConnector().GetDB().Collection("numbers")
	numbersCollection.FindOne(context.Background(), bson.D{{"number", clientreq.Number}}).Decode(&res)

	var apiresponse *QueryNumberResponse

	if res.Id != primitive.NilObjectID {
		foundbyfoundat := make(map[string]time.Time)
		for i := range res.Guessers {
			foundbyfoundat[res.Guessers[i]] = time.Unix(res.Datetime[i], 0)
		}

		apiresponse = &QueryNumberResponse{
			Result: "success",
			Error:  "",
			ExtraDetails: &QueryNumberExtraDetails{
				Number:         res.Number,
				IsActive:       res.IsActive,
				FoundByFoundAt: foundbyfoundat,
			},
		}
	} else {
		apiresponse = &QueryNumberResponse{
			Result: "success",
			Error:  "doesn't exist",
		}
	}

	returnApiJsonResult(writer, apiresponse)
}

type QueryGuesserRequest struct {
	GuesserId string
}

type QueryGuesserExtraDetails struct {
	GuesserId    string `json:"guesser_id"`
	IsActive     bool   `json:"is_active"`
	FoundWhoWhen map[string]time.Time
}

type QueryGuesserResponse struct {
	Result       string                    `json:"result"`
	Error        string                    `json:"error,omitempty"`
	ExtraDetails *QueryGuesserExtraDetails `json:"extra_details,omitempty"`
}

func QueryGuesser(writer http.ResponseWriter, request *http.Request) {
	dec := UnpackBody(request)
	var clientrequest QueryGuesserRequest

	for {

		if err := dec.Decode(&clientrequest); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("API: {\n\tGuesserId:\t%s\n}\n", clientrequest.GuesserId)
	}

	// Query mongo for guesser
	var res guessers.GuesserDb
	numbersCollection := mongowrapper.NewMongoConnector().GetDB().Collection("guessers")
	numbersCollection.FindOne(context.Background(), bson.D{{"guesser_id", clientrequest.GuesserId}}).Decode(&res)

	var apiresponse *QueryGuesserResponse

	if res.Id != primitive.NilObjectID {
		foundbyfoundat := make(map[string]time.Time)
		for i := range res.GoodGuesses {
			foundbyfoundat[res.GoodGuesses[i]] = time.Unix(res.Datetime[i], 0)
		}

		apiresponse = &QueryGuesserResponse{
			Result: "success",
			Error:  "",
			ExtraDetails: &QueryGuesserExtraDetails{
				GuesserId:    res.GuesserId,
				IsActive:     res.IsActive,
				FoundWhoWhen: foundbyfoundat,
			},
		}
	} else {
		apiresponse = &QueryGuesserResponse{
			Result: "success",
			Error:  "doesn't exist",
		}
	}

	returnApiJsonResult(writer, apiresponse)
}

type QueryPrimeResponse struct {
	Result string                    `json:"result"`
	Primes []*QueryPrimeExtraDetails `json:"primes"`
}

type QueryPrimeExtraDetails struct {
	Number      int                  `json:"number"`
	WhoWhatWhen []*GuesserNumberWhen `json:"who_what_when"`
}
type GuesserNumberWhen struct {
	Guesser           string    `json:"guesser"`
	OriginatingNumber int       `json:"originating_number"`
	When              time.Time `json:"when"`
}

func QueryPrimes(writer http.ResponseWriter, _ *http.Request) {
	fmt.Printf("API: \nQuery Primes\n")

	// Query mongo for guesser
	primesCollection := mongowrapper.NewMongoConnector().GetDB().Collection("primes")

	ctx := context.Background()
	cursor, err := primesCollection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(ctx)
	apiresponse := &QueryPrimeResponse{
		Result: "success",
		Primes: []*QueryPrimeExtraDetails{},
	}
	for cursor.Next(ctx) {
		var res tasks.PrimeDb
		if err = cursor.Decode(&res); err != nil {
			log.Fatal(err)
		}
		fmt.Println(res)

		primeQueryRecord := &QueryPrimeExtraDetails{
			Number:      res.Number,
			WhoWhatWhen: []*GuesserNumberWhen{},
		}
		for i := range res.OriginGuesser {
			recordToInsert := &GuesserNumberWhen{
				Guesser:           res.OriginGuesser[i],
				OriginatingNumber: res.OriginNumber[i],
				When:              time.Unix(res.Datetime[i], 0),
			}
			primeQueryRecord.WhoWhatWhen = append(primeQueryRecord.WhoWhatWhen, recordToInsert)
		}

		apiresponse.Primes = append(apiresponse.Primes, primeQueryRecord)
	}

	returnApiJsonResult(writer, apiresponse)
}

func UnpackBody(request *http.Request) *json.Decoder {
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}

	log.Println("API: " + string(body))
	dec := json.NewDecoder(strings.NewReader(string(body)))
	return dec
}

type AddGuesserRequest struct {
	BeginAt         int32
	IncrementBy     int32
	SleepMs         int32
	ResponseChannel chan *AddGuesserResponse `json:"omitempty"`
}

type AddGuesserResponse struct {
	Result    string `json:"result"`
	Error     string `json:"error,omitempty"`
	GuesserId string `json:"guesser_id,omitempty"`
}

func AddGuesser(writer http.ResponseWriter, request *http.Request) {
	dec := UnpackBody(request)
	responsechannel := make(chan *AddGuesserResponse)
	for {
		var protoReq AddGuesserRequest
		if err := dec.Decode(&protoReq); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		if protoReq.SleepMs > 1000*60*2 {
			returnApiJsonResult(writer, AddGuesserResponse{
				Result: "failure",
				Error:  "Sleep time longer than 2 mins",
			})
			return
		}
		fmt.Printf("API: {\n\tBeginAt:\t%d\n\tIncrementBy:\t%d\n\tSleep in MS:\t%d\n}\n", protoReq.BeginAt, protoReq.IncrementBy, protoReq.SleepMs)
		protoReq.ResponseChannel = responsechannel
		createGuesserChannel <- protoReq
	}
	returnApiJsonResult(writer, <-responsechannel)
}

type RemoveGuesserRequest struct {
	GuesserId       string
	ClearDb         bool                        `json:"ClearDb,omitempty"`
	ResponseChannel chan *RemoveGuesserResponse `json:"omitempty"`
}

type RemoveGuesserResponse struct {
	Result string `json:"result"`
	Error  string `json:"error,omitempty"`
}

func RemoveGuesser(writer http.ResponseWriter, request *http.Request) {
	dec := UnpackBody(request)
	responsechannel := make(chan *RemoveGuesserResponse)
	for {
		var protoReq RemoveGuesserRequest
		if err := dec.Decode(&protoReq); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("API: {\n\tGuesserID:\t%s\n\tClearDb:\t%t\n}\n", protoReq.GuesserId, protoReq.ClearDb)
		protoReq.ResponseChannel = responsechannel
		removeGuesserChannel <- protoReq
	}
	returnApiJsonResult(writer, <-responsechannel)
}

type RemoveNumberRequest struct {
	Number          int32
	ClearDbRecord   bool                       `json:"ClearDbRecord,omitempty"`
	ResponseChannel chan *RemoveNumberResponse `json:"omitempty"`
}

type RemoveNumberResponse struct {
	Result string `json:"result"`
	Error  string `json:"error,omitempty"`
}

func RemoveNumber(writer http.ResponseWriter, request *http.Request) {
	dec := UnpackBody(request)
	responsechannel := make(chan *RemoveNumberResponse)
	for {
		var protoReq RemoveNumberRequest
		if err := dec.Decode(&protoReq); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("API: {\n\tNumber:\t%d\n\tClearDb:\t%t\n}\n", protoReq.Number, protoReq.ClearDbRecord)
		protoReq.ResponseChannel = responsechannel
		removeNumberChannel <- protoReq
	}
	returnApiJsonResult(writer, <-responsechannel)
}

type AddNumberRequest struct {
	Number          int32
	ResponseChannel chan *AddNumberResponse `json:"omitempty"`
}

type AddNumberResponse struct {
	Result string `json:"result"`
	Error  string `json:"error,omitempty"`
}

func AddNumber(writer http.ResponseWriter, request *http.Request) {
	dec := UnpackBody(request)
	responsechannel := make(chan *AddNumberResponse)
	for {
		var protoReq AddNumberRequest
		if err := dec.Decode(&protoReq); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("API: {\n\tNumber:\t%d\n}\n", protoReq.Number)
		protoReq.ResponseChannel = responsechannel
		addNumberChannel <- protoReq
	}

	returnApiJsonResult(writer, <-responsechannel)
}

func returnApiJsonResult(writer http.ResponseWriter, apiresponse interface{}) {
	js, err := json.Marshal(apiresponse)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
	writer.Write(js)
}
