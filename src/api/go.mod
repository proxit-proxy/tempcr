module src/api

go 1.16

replace src/pkg => ./../pkg

replace src/guessers => ./../guessers

replace src/numbers => ./../numbers

replace src/api/ => ./api/

require (
	go.mongodb.org/mongo-driver v1.5.4
	google.golang.org/grpc v1.39.0
	src/guessers v0.0.0-00010101000000-000000000000
	src/numbers v0.0.0-00010101000000-000000000000
	src/pkg v0.0.0-00010101000000-000000000000
)
