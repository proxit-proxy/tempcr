package main

import (
	"os"
	"src/api/api"
)

func main() {
	os.Exit(api.RealMain())
}
