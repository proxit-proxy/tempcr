package main

import (
	"os"
	"src/taskworker/taskworker"
)

func main() {
	os.Exit(taskworker.RealMain())
}
