module src/taskworker

go 1.15

replace src/pkg => ./../pkg

require (
	github.com/RichardKnop/machinery v1.10.6
	go.uber.org/zap v1.18.1
	src/pkg v0.0.0-00010101000000-000000000000
)
