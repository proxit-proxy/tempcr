package taskworker

import (
	"fmt"
	"github.com/RichardKnop/machinery/v1"
	"go.uber.org/zap"
	"log"
	"net/http"
	_ "net/http/pprof"
	mcnServer "src/pkg/asynctasks/server"
)

func StartWorkerServer(server *machinery.Server) {
	worker := server.NewWorker("default", 1000)
	err := worker.Launch()
	if err != nil {
		log.Panic(err)
	}
}

func RealMain() int {
	fmt.Println("Taskworker: Starting machinery taskworker")
	// add pprof endpoint for online production profiling
	go func() {
		err := http.ListenAndServe("localhost:6060", nil)
		if err != nil {
			log.Fatalf("error running pprof http server", zap.String("error", err.Error()))
		}
	}()

	server, err := mcnServer.NewServer(nil)
	if err != nil {
		log.Panic(err)
	}
	errs := mcnServer.RegisterTasks(server)
	if errs != nil {
		// handle?
	}
	StartWorkerServer(server)
	return 1
}
