package main

import (
	"os"
	"src/guessers/guessers"
)

func main() {
	os.Exit(guessers.RealMain())
}
