package guessers

import (
	"fmt"
	guesserpb "src/guessers/pb"
	"time"
)

type guesser struct {
	sleepTime int32
	incBy int32
	beginAt int32
	killChan chan *guesserpb.RemoveGuesserRequest
	guesserId string
	serverId string
	guessResponse chan *guesserpb.GuessResponse
	guessChannel chan *guesserpb.GuessRequest
}

func NewGuesser(sleepTime int32, incBy int32, beginAt int32, killChan chan *guesserpb.RemoveGuesserRequest, guesserId string, serverId string, guessResponse chan *guesserpb.GuessResponse, guessChannel chan *guesserpb.GuessRequest, guesserdeletechannel chan string, guesserinactivatechannel chan string) *guesser {
	return &guesser{
		sleepTime:     sleepTime,
		incBy:         incBy,
		beginAt:       beginAt,
		killChan:      killChan,
		guesserId:     guesserId,
		serverId:      serverId,
		guessResponse: guessResponse,
		guessChannel:  guessChannel,
	}
}

func (g *guesser) Start() {
	successes := 0
	for {
		fmt.Printf("Guesser: Guess %v sending request\n", g.guesserId)
		// Send request
		g.guessChannel <- &guesserpb.GuessRequest{
			GuesserId:     g.guesserId,
			ServerId:      g.serverId,
			GuessedNumber: g.beginAt,
		}
		// Should we timeout?
		response := <-g.guessResponse

		if response == nil || !response.Ok {
			continue
		}
		fmt.Printf("Guesser: Guess %v received response of correct guess = %t\n", g.guesserId, response.SuccessfulGuess)
		if response.SuccessfulGuess {
			successes++
		}
		g.beginAt += g.incBy

		select {
		case killrequest := <-g.killChan:
			if killrequest.ClearDbRecord {
				guesserdeletechannel <- g.guesserId
			} else {
				guesserinactivatechannel <- g.guesserId
			}
			return
		default:
			fmt.Printf("Guesser: Guess %v Sleep\n", g.guesserId)
			time.Sleep(time.Millisecond * time.Duration(g.sleepTime))
		}
	}
}


