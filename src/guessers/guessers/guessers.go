package guessers

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
	guesserpb "src/guessers/pb"
	"src/pkg/asynctasks/client"
	"src/pkg/consul"
	"src/pkg/mongowrapper"
	"strconv"
	"time"
)

type Guesser struct {
	BeginAt     int
	IncrementBy int
	SleepMs     int
	GuesserId   string
}

var guesserId int

type server struct {
	guesserpb.UnimplementedGuesserServiceServer
}

// Mapping
var guesserIdToChannel map[string]chan *guesserpb.GuessResponse
var killChannels map[string]chan *guesserpb.RemoveGuesserRequest
var guessChannel chan *guesserpb.GuessRequest
var guesserdeletechannel chan string
var guesserinactivatechannel chan string


var serverId string

const guessersChunkSize = 10

const grpcport string = "50052"

func RealMain() int {
	// Init
	serverId = uuid.New().String() // Change to host or something
	fmt.Println("Guessers: Generated id for self:", serverId)
	GetLatestGuesserId()
	consul.RegisterServiceConsul("guessers", grpcport)
	// Channels for guessers
	guesserIdToChannel = make(map[string]chan *guesserpb.GuessResponse)
	killChannels = make(map[string]chan *guesserpb.RemoveGuesserRequest)
	guessChannel = make(chan *guesserpb.GuessRequest)
	guesserdeletechannel = make(chan string)
	guesserinactivatechannel = make(chan string)


	go guessersDbManager(guesserdeletechannel, guesserinactivatechannel)
	fmt.Println("Guessers: Guessers server")

	registered := make(chan bool)
	// Listening to API server
	go GuessServerListen(registered)

	go keepAPIStreamAlive(registered)

	for {

	}
}

func GuessServerListen(registered chan bool) {
	registered <- true
	// Creation / Removal Server / API server is alive
	lis, err := net.Listen("tcp", "0.0.0.0:"+grpcport)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	guesserpb.RegisterGuesserServiceServer(s, &server{})
	fmt.Println("Guessers: GuessServerListen")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	fmt.Println("Guessers: GuessServerListen exit")
}

func keepAPIStreamAlive(registered chan bool) {
	<-registered
	for {
		resurrect := make(chan bool)
		go ApiServerStreamGuesses(resurrect)
		<-resurrect
	}
}
func ApiServerStreamGuesses(resurrect chan bool) {
	var gatewayStream guesserpb.Gateway_GuessClient
	var guesserscc *grpc.ClientConn

	var err error

	// Connect to API server
	for {
		fmt.Println("Guessers: Trying client to API server")
		// Guess Client to API gateway
		client := consul.CallService("api")
		guesserGatewayClient := guesserpb.NewGatewayClient(client)
		gatewayStream, err = guesserGatewayClient.Guess(context.Background())

		if err != nil {
			if e, ok := status.FromError(err); ok {
				switch e.Code() {
				case codes.Unavailable:
					fmt.Println("Guessers: API Gateway not ready for guess streaming, retrying in 5 seconds")
					time.Sleep(time.Second * time.Duration(5))
					continue
				case codes.Unimplemented:
					fmt.Println("Guessers: API Gateway not ready for guess streaming, retrying in 5 seconds")
					time.Sleep(time.Second * time.Duration(5))
					continue
				default:
					fmt.Println("Guessers: Sparta", e.Code(), e.Message())
				}
				log.Fatalf("Guessers: Error while opening connection to API for guesses: %v", err)
			}
		} else {
			break
		}
	}

	destroychan := make(chan bool)
	// Receive from gateway
	go func(gatewayStream guesserpb.Gateway_GuessClient, destorychan chan bool, resurrect chan bool) {
		fmt.Println("Guessers: Starting Guess RPC receiver")

		for {
			// switch
			req, err := gatewayStream.Recv()
			if err == io.EOF {
				guesserscc.Close()
				guesserIdToChannel[req.GuesserId] <- &guesserpb.GuessResponse{ Ok: false}
				destroychan <- true
				resurrect <- true
				return
			}
			if err != nil {
				if req != nil {
					guesserIdToChannel[req.GuesserId] <- &guesserpb.GuessResponse{Ok: false}
				}
				destroychan <- true
				resurrect <- true
				return
			}
			guesserIdToChannel[req.GuesserId] <- req
		}
	}(gatewayStream, destroychan, resurrect)
	// Send to gateway
	go func(gatewayStream guesserpb.Gateway_GuessClient, guessChannel chan *guesserpb.GuessRequest, destorychan chan bool) {
		fmt.Println("Guessers: Starting Guess RPC sender")

		for {
			// Switch
			select {
			case req := <-guessChannel:
				err := gatewayStream.Send(req)
				if err != nil {
					destroychan <- true
					fmt.Printf("Guessers: Received error when trying to send guess to gateway: %v\n", err)
				}
			case <-destroychan:
				return
			}
		}
	}(gatewayStream, guessChannel, destroychan)
}

func (*server) CreateGuesser(ctx context.Context, req *guesserpb.CreateGuesserRequest) (*guesserpb.CreateGuesserResponse, error) {
	fmt.Printf("Guessers: Create guesser %v\n", req)

	sleepTime := req.SleepFor
	incBy := req.IncrementBy
	beginAt := req.BeginAt
	guesserIdStr := strconv.Itoa(guesserId)
	killChannels[guesserIdStr] = make(chan *guesserpb.RemoveGuesserRequest)
	guesserIdToChannel[guesserIdStr] = make(chan *guesserpb.GuessResponse)

	// Add user to db
	if !AddGuesserToDb(ctx, time.Now(), guesserIdStr) {
		return &guesserpb.CreateGuesserResponse{
			Ok:        false,
			GuesserId: "N/A",
		}, nil
	}

	response := &guesserpb.CreateGuesserResponse{GuesserId: strconv.Itoa(guesserId), Ok: true}

	guesser := NewGuesser(sleepTime, incBy, beginAt, killChannels[guesserIdStr], guesserIdStr, serverId, guesserIdToChannel[guesserIdStr], guessChannel, guesserdeletechannel, guesserinactivatechannel)
	go guesser.Start()

	go updateGuesserId()
	guesserId++
	// If we need another chunk
	if guesserId%guessersChunkSize == 0 {
		GetLatestGuesserId()
	}

	return response, nil
}

func guessersDbManager(guesserdeletechannel chan string, guesserinactivatechannel chan string) {
	for {
		select {
		case guesserid := <-guesserdeletechannel:
			fmt.Println("guessersDbManager: Received delete", guesserid)
			DeleteGuesserFromDb(guesserid)
			delete(guesserIdToChannel, guesserid)
		case guesserid := <-guesserinactivatechannel:
			fmt.Println("guessersDbManager: Received inactivate", guesserid)
			InactivateGuesserInDb(guesserid)
			delete(guesserIdToChannel, guesserid)
		default:
			time.Sleep(time.Second)
		}
	}
}
func DeleteGuesserFromDb(guesserId string) {
	guesserscollection := mongowrapper.NewMongoConnector().GetDB().Collection("guessers")
	guesserscollection.DeleteOne(context.Background(), bson.D{{"guesser_id", guesserId}})
}
func InactivateGuesserInDb(guesserId string) {
	guesserscollection := mongowrapper.NewMongoConnector().GetDB().Collection("guessers")
	guesserscollection.UpdateOne(context.Background(), bson.D{{"guesser_id", guesserId}}, bson.D{{"$set", bson.M{"is_active": false}}})
}

type GuessersIndex struct {
	Id    primitive.ObjectID `bson:"_id"`
	Index int                `bson:"index"`
}

func GetLatestGuesserId() {
	guessersidcollection := mongowrapper.NewMongoConnector().GetDB().Collection("guessid")

	opts := options.FindOneAndUpdate().SetUpsert(true)
	filter := bson.D{{"index", bson.M{"$exists": true}}}
	update := bson.D{{"$inc", bson.M{"index": 10}}}
	var updatedDocument GuessersIndex
	err := guessersidcollection.FindOneAndUpdate(context.TODO(), filter, update, opts).Decode(&updatedDocument)
	if err != nil {
		// ErrNoDocuments means that the filter did not match any documents in the collection
		if err == mongo.ErrNoDocuments {
			return
		}
		log.Fatal(err)
	}
	guesserId = updatedDocument.Index

}

func updateGuesserId() {
	_, err := client.UpdateServerGuesserId(serverId, guesserId)
	if err != nil {
		fmt.Println("Guessers: Failed updating latest guessers id for (serverId, guesserId):", serverId, guesserId)
	}
}

type GuesserDb struct {
	Id            primitive.ObjectID `bson:"_id"`
	GuesserId     string             `bson:"guesser_id"`
	GoodGuesses   []string           `bson:"good_guesses"`
	CreatedTime   int64              `bson:"created_time"`
	AttemptNumber []int32            `bson:"attempt_number"`
	Datetime      []int64            `bson:"datetime"`
	IsActive      bool               `bson:"is_active"`
}

func AddGuesserToDb(ctx context.Context, creationTime time.Time, guesserId string) bool {
	guesserscollection := mongowrapper.NewMongoConnector().GetDB().Collection("guessers")
	var res GuesserDb
	// Exists
	guesserscollection.FindOne(ctx, bson.D{{"guesser_id", guesserId}}).Decode(&res)
	if res.Id != primitive.NilObjectID {
		return false
	}

	doc := &GuesserDb{
		Id:            primitive.NewObjectID(),
		GuesserId:     guesserId,
		GoodGuesses:   []string{},
		CreatedTime:   creationTime.Unix(),
		AttemptNumber: []int32{},
		Datetime:      []int64{},
		IsActive:      true,
	}

	addguesserres, err := guesserscollection.InsertOne(context.Background(), doc)
	if err != nil {
		fmt.Printf("Guessers: Add guesser mongo error %v\n", err)
		return false
	}
	fmt.Printf("Guessers: Add guesser mongo result %v\n", addguesserres)
	return true
}

func (*server) RemoveGuesser(_ context.Context, req *guesserpb.RemoveGuesserRequest) (*guesserpb.RemoveGuesserResponse, error) {
	fmt.Printf("Guessers: Invoked remove guesser %v\n", req)

	// Send kill to guesser
	if _, ok := guesserIdToChannel[req.GuesserId]; ok {
		go func(killChannels map[string]chan *guesserpb.RemoveGuesserRequest, req *guesserpb.RemoveGuesserRequest) {
			channel := killChannels[req.GuesserId]
			delete(killChannels, req.GuesserId)
			channel <- req
		}(killChannels, req)
		return &guesserpb.RemoveGuesserResponse{Ok: true}, nil
	} else {
		fmt.Printf("Guessers: Remove guesser %v, guesser doesnt exist\n", req)
		return &guesserpb.RemoveGuesserResponse{Ok: false}, nil
	}
}
