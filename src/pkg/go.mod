module src/pkg

go 1.16

require (
	github.com/RichardKnop/machinery v1.10.6
	github.com/go-redis/redis/v8 v8.6.0
	github.com/hashicorp/consul/api v1.9.1
	go.mongodb.org/mongo-driver v1.5.4
	go.uber.org/zap v1.18.1
	google.golang.org/grpc v1.35.0
)
