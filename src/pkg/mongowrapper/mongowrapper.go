package mongowrapper

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"log"
	"sync"
	"time"
)

/* Move to another file */
type MongoConnector struct{}

var once sync.Once
var singletonClient *mongo.Client

func NewMongoConnector() *MongoConnector {
	connector := &MongoConnector{}
	return connector
}

func (*MongoConnector) connect() (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// explicit default client level options

	opts := options.Client().ApplyURI("mongodb://172.38.0.27:27001,172.38.0.27:27002,172.38.0.27:27003/?replicaSet=rs0")
	opts = opts.SetReadConcern(readconcern.Majority()).SetWriteConcern(writeconcern.New(writeconcern.WMajority()))

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (m *MongoConnector) GetDB() *mongo.Database {
	once.Do(func() {
		client, err := m.connect()
		if err != nil {
			log.Panicf("error connecting to DB, panicking. error: %v", err)
		}
		singletonClient = client
	})
	return singletonClient.Database("MockTask")
}

func (m *MongoConnector) RunTransaction(collections []string, filters map[string]bson.D, changes map[string]bson.D) bool {
	wc := writeconcern.New(writeconcern.WMajority())

	rc := readconcern.Snapshot()

	txnOpts := options.Transaction().SetWriteConcern(wc).SetReadConcern(rc)

	session, err := singletonClient.StartSession()

	if err != nil {
		panic(err)
	}

	defer session.EndSession(context.Background())

	callback := func(sessionContext mongo.SessionContext) (interface{}, error) {
		var result *mongo.UpdateResult
		var err error

		for _, collectionname := range collections {
			collection := m.GetDB().Collection(collectionname)
			filter := filters[collectionname]
			change := changes[collectionname]

			result, err = collection.UpdateOne(sessionContext, filter, change)
			if err != nil {
				return nil, err
			}
		}
		return result, err
	}

	_, err = session.WithTransaction(context.Background(), callback, txnOpts)

	if err != nil {
		return false
	}
	return true
}
