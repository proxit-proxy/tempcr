package consul

import (
	"bufio"
	"errors"
	"fmt"
	consulapi "github.com/hashicorp/consul/api"
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func ExtractPort(portsuffix string, portvariable *string) {
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		fmt.Println(pair[0], "=", pair[1])
		if strings.HasSuffix(pair[0], "PORT_"+portsuffix) {
			*portvariable = pair[1]
			fmt.Println("\nSo.... I've found", pair[0], " = ", pair[1])
		}
	}
}

func RegisterServiceConsul(servicename string, port string) {
	registration := new(consulapi.AgentServiceRegistration)

	address := OwnIP()

	registration.ID = servicename + "_" + strings.Split(address, ".")[3]
	registration.Name = servicename

	registration.Address = address
	registration.Port, _ = strconv.Atoi(port)
	registration.Check = new(consulapi.AgentServiceCheck)
	registration.Check.TCP = fmt.Sprintf("%s:%s", address, port)
	registration.Check.Interval = "5s"
	registration.Check.Timeout = "3s"

	time.Sleep(5 * time.Second)
	consul, err := consulClient()
	if err != nil {
		return
	}
	fmt.Println("Registering", servicename, "With address", address, ",Port:", port)
	consul.Agent().ServiceRegister(registration)
}

func OwnIP() string {

	file, _ := os.Open("/etc/hosts")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lastword := ""
	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Split(line, "\t")
		if len(words) > 1 {
			lastword = words[0]
		}
	}

	return lastword
}
func CallNextServiceInstance(tried map[string]bool, servicename string) (*grpc.ClientConn, string) {
	addresses, _ := lookupServiceWithConsul(servicename)
	for _, address := range addresses {
		if _, ok := tried[address]; !ok {
			return connectToAddress(address), address
		}
	}
	fmt.Println("I havent found any services of", servicename)
	return nil, ""
}

func CallService(servicename string) *grpc.ClientConn {
	addresses, _ := lookupServiceWithConsul(servicename)
	address := addresses[rand.Intn(len(addresses))]
	return connectToAddress(address)
}

func connectToAddress(address string) *grpc.ClientConn {
	clientconnection, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	return clientconnection
}

func lookupServiceWithConsul(serviceName string) ([]string, error) {
	attempts := 5
GetClient:
	consul, err := consulClient()
	if err != nil {
		attempts--
		if attempts > 0 {
			goto GetClient // I know I'm lazy because performance is not an issue :P
		}
		return nil, err
	}

	services, err := consul.Agent().Services()
	if err != nil {
		fmt.Println("Failed in consul due to: ", err)
		return nil, err
	}

	var results []string
	for k, v := range services {
		if strings.HasPrefix(k, serviceName) {
			address := v.Address
			port := v.Port

			result := fmt.Sprintf("%s:%v", address, port)
			results = append(results, result)
		}
	}

	if results == nil {
		attempts--
		if attempts > 0 {
			goto GetClient // I know I'm lazy because performance is not an issue :P
		}
		return nil, errors.New("Service called " + serviceName + " not found")
	}
	fmt.Printf("Got services: %v\n", results)
	return results, nil
}

func consulClient() (*consulapi.Client, error) {
	config := consulapi.DefaultConfig()

	config.Address = "consul-server:8500"
	client, err := consulapi.NewClient(config)
	if err != nil {
		fmt.Println("Failed in consul due to: ", err)
		return nil, err
	}
	return client, nil
}
