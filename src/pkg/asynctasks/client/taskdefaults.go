package client

import "github.com/RichardKnop/machinery/v1/tasks"

func SetDefaultTaskSignature(sig *tasks.Signature, queue string) {
	if queue != "" {
		sig.RoutingKey = queue
	}
	sig.RetryCount = 3
}
