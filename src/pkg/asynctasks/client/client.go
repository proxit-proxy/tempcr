package client

import (
	"github.com/RichardKnop/machinery/v1/backends/result"
	"github.com/RichardKnop/machinery/v1/tasks"
	"src/pkg/asynctasks/server"
)

func SendFindClosestPrime(startFrom int, originGuesser string) (*result.AsyncResult, error) {

	args := []tasks.Arg{{
		Name:  "startFrom",
		Type:  "int",
		Value: startFrom,
	}, {
		Name:  "originGuesser",
		Type:  "string",
		Value: originGuesser,
	}}
	sig, _ := tasks.NewSignature("SearchForPrime", args)
	SetDefaultTaskSignature(sig, "default")

	return server.SendTask(sig)
}

func UpdateServerGuesserId(serverId string, guesserId int) (*result.AsyncResult, error) {

	args := []tasks.Arg{{
		Name:  "serverId",
		Type:  "string",
		Value: serverId,
	}, {
		Name:  "guesserId",
		Type:  "int",
		Value: guesserId,
	}}
	sig, _ := tasks.NewSignature("UpdateServerGuesserId", args)
	SetDefaultTaskSignature(sig, "default")

	return server.SendTask(sig)
}
