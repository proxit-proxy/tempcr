package tasks

import (
	"fmt"
	"reflect"
)

type taskNames struct {
	SearchForPrime        string
	UpdateServerGuesserId string
}

var TaskNames = taskNames{SearchForPrime: "SearchForPrime", UpdateServerGuesserId: "UpdateServerGuesserId"}

func GetTaskNames() *taskNames {
	return &TaskNames
}

type PeriodicTask struct {
	Name string
	Spec string
}

// GetPeriodicTasks This function uses reflection run over all members of TaskNames, checks if they are priodic,
// and returns a slice of all periodic tasks in it
func GetPeriodicTasks() []PeriodicTask {
	tasks := make([]PeriodicTask, 0, 10)
	namesrefl := reflect.ValueOf(TaskNames)
	for i := 0; i < namesrefl.NumField(); i++ {

		// get name of task
		nameField := namesrefl.Field(i)
		if !nameField.CanInterface() {
			continue
		}
		name := nameField.Interface()

		// get tag of task, check if it's periodic and extract spec if it is
		nameFieldTag := namesrefl.Type().Field(i).Tag
		spec, exists := nameFieldTag.Lookup("spec")

		// if spec tag exists, it's a periodic task
		if exists {
			tasks = append(tasks, PeriodicTask{Name: fmt.Sprint(name), Spec: spec})
		}
	}
	return tasks
}
