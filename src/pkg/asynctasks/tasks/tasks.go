package tasks

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"src/pkg/mongowrapper"
	"time"
)

type PrimeDb struct {
	Id            primitive.ObjectID `bson:"_id"`
	Number        int                `bson:"number"`
	OriginNumber  []int              `bson:"origin_number"`
	Datetime      []int64            `bson:"datetime"`
	OriginGuesser []string           `bson:"origin_guesser"`
}

func (*TaskContainer) SearchForPrime(startFrom int, originGuesser string) error {
	origin := startFrom
	now := time.Now()
OuterLoop:
	for {
		for i := 2; i < startFrom; i++ {
			if startFrom%i == 0 {
				startFrom++
				continue OuterLoop
			}
		}
		// Add to db
		primesCollection := mongowrapper.NewMongoConnector().GetDB().Collection("primes")
		ctx := context.Background()
		var res PrimeDb
		primesCollection.FindOne(ctx, bson.D{{"number", startFrom}}).Decode(&res)

		if res.Id != primitive.NilObjectID {
			fmt.Println("Primes Task: Found", res)

			updateres, err := primesCollection.UpdateOne(ctx, bson.D{{"_id", res.Id}},
				bson.D{{"$push",
					bson.M{
						"datetime":       now.Unix(),
						"origin_guesser": originGuesser,
						"origin_number":  origin,
					}}})
			if err != nil {
				return err
			}
			fmt.Printf("Primes Task: Add Number mongo result %v\n", updateres)
			return nil
		}

		doc := &PrimeDb{
			Id:            primitive.NewObjectID(),
			Number:        startFrom,
			OriginNumber:  []int{origin},
			Datetime:      []int64{now.Unix()},
			OriginGuesser: []string{originGuesser},
		}

		addNumberRes, err := primesCollection.InsertOne(ctx, doc)
		if err != nil {
			fmt.Printf("Primes Task: Add Number mongo error %v\n", err)
			return err
		}
		fmt.Printf("Primes Task: Add Number mongo result %v\n", addNumberRes)
		return nil
	}
}

type ServerGuesserId struct {
	Id        primitive.ObjectID `bson:"_id"`
	GuesserId []int              `bson:"guesser_id"`
	ServerId  string             `bson:"server_id"`
}

func (*TaskContainer) UpdateServerGuesserId(serverId string, guesserId int) error {
	guessersidcollection := mongowrapper.NewMongoConnector().GetDB().Collection("guessid")

	filter := bson.M{"server_id": serverId}
	upsert := true
	opts := &options.UpdateOptions{Upsert: &upsert}

	_, err := guessersidcollection.UpdateOne(context.Background(), filter, bson.D{{"$push", bson.M{"guesser_id": guesserId}}}, opts)

	return err
}
