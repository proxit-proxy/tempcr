package server

import (
	"errors"
	"fmt"
	"github.com/RichardKnop/machinery/v1"
	"github.com/RichardKnop/machinery/v1/backends/result"
	mcnconf "github.com/RichardKnop/machinery/v1/config"
	mcntasks "github.com/RichardKnop/machinery/v1/tasks"
	"log"
	"reflect"
	"src/pkg/asynctasks/tasks"
	"sync"
)

func NewServer(icnf *mcnconf.Config) (*machinery.Server, error) {
	var defaultCnf = &mcnconf.Config{
		Broker:        "redis://redis:6379", //"redis://172.38.0.63:6379/", // "config.GetCommonConfig().RedisConnectionString", // TODO: FIX
		DefaultQueue:  "default",
		ResultBackend: "redis://redis:6379", //"redis://172.38.0.63:6379/", // "config.GetCommonConfig().RedisConnectionString", // TODO: FIX
		Lock:          "redis://redis:6379", //"redis://172.38.0.63:6379/", // "config.GetCommonConfig().RedisConnectionString", // TODO: FIX
		Redis: &mcnconf.RedisConfig{
			MaxIdle:                3,
			IdleTimeout:            240,
			ReadTimeout:            15,
			WriteTimeout:           15,
			ConnectTimeout:         15,
			NormalTasksPollPeriod:  1000,
			DelayedTasksPollPeriod: 500,
		},
	}

	var conf *mcnconf.Config
	if icnf != nil {
		conf = icnf
	} else {
		conf = defaultCnf
	}
	server, err := machinery.NewServer(conf)
	return server, err
}

var onceServer sync.Once
var taskServer *machinery.Server

// Currently Singleton, could potentially be refactored to DI in the future
// Using default values if cnf is nil
func GetServer(icnf *mcnconf.Config) *machinery.Server {
	var err error
	onceServer.Do(func() {
		taskServer, err = NewServer(icnf)
		if err != nil {
			return
		}
		log.Print(RegisterTasks(taskServer))
	})
	if err != nil {
		// panicking here since it represents misconfiguration, and not communication error
		log.Panicf("error creating machinery server instance, err: %v", err)
	}

	return taskServer
}

// This function uses reflection run over all methods of TaskContainer, match their name with the task names
// existing in the file name.go under tasks, and for every method matching a name, register it onto the server.
func RegisterTasks(server *machinery.Server) []error {
	var errs []error
	names := tasks.GetTaskNames()
	namesrefl := reflect.ValueOf(*names)
	for i := 0; i < namesrefl.NumField(); i++ {
		nameField := namesrefl.Field(i)
		if !nameField.CanInterface() {
			continue
		}
		name := nameField.Interface()
		log.Print(fmt.Sprint(name))
		log.Print(reflect.ValueOf(&tasks.TaskContainer{}).NumMethod())
		log.Print(reflect.ValueOf(&tasks.TaskContainer{}).Method(0))
		method := reflect.ValueOf(&tasks.TaskContainer{}).MethodByName(fmt.Sprint(name))
		if !method.IsValid() || !method.CanInterface() {
			message := fmt.Sprintf("function with name %v found, but no method found in task container", name)
			errs = append(errs, errors.New(message))
			log.Print(message)
			continue
		}
		err := server.RegisterTask(fmt.Sprint(name), method.Interface())
		if err != nil {
			errs = append(errs, err)
			log.Printf("failed to register task with name %v, error: %v", name, err)
		}
	}
	return errs
}

func SendTask(signature *mcntasks.Signature) (*result.AsyncResult, error) {
	srv := GetServer(nil)
	res, err := srv.SendTask(signature)
	return res, err
}
