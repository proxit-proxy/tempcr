package redis

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
	"sync"
)

var once sync.Once
var singletonClient redis.UniversalClient

func remoteRedisConnect() (redis.UniversalClient, error) {
	// TODO: move from parse url to sentinel parameters that will make universal client spawn a failover client
	dialOptions, err := redis.ParseURL("redis://redis:6379/")
	if err != nil {

		return nil, err
	}
	uniOptions := &redis.UniversalOptions{Addrs: []string{dialOptions.Addr},
		Username: dialOptions.Username,
		Password: dialOptions.Password,
		DB:       dialOptions.DB}
	redisClient := redis.NewUniversalClient(uniOptions)

	return redisClient, nil
}

func GetRedis(clients ...redis.UniversalClient) redis.UniversalClient {
	fmt.Println("Redis: called GetRedis")
	once.Do(func() {
		fmt.Println("Redis: Singleton client creation")
		var client redis.UniversalClient
		var err error
		// If passed a client, update the singleton to hold that client.
		if len(clients) > 0 {
			client = clients[0]
		} else {
			// otherwise, get remote redis
			client, err = remoteRedisConnect()
			if err != nil {
				fmt.Println("error connecting to redis", zap.String("error", err.Error()))
			}
		}
		singletonClient = client
	})
	fmt.Println("Redis: Returning client")
	return singletonClient
}
