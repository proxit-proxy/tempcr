package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

type RedisAttemptSession struct {
	GenericRedisBackend
	client redis.UniversalClient
}

func NewRedisAttemptSession() *RedisAttemptSession {
	return &RedisAttemptSession{client: GetRedis()}
}

type AttemptSession struct {
	GuesserId       string
	GuesserServerId string
	Attempt         int32
}

func (p *AttemptSession) String() string {
	return fmt.Sprintf("guesserattempt:%s:%s", p.GuesserServerId, p.GuesserId)
}

func (ps *RedisAttemptSession) GetAndInc(e *AttemptSession) int32 {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	attempt, err := ps.client.Incr(ctx, e.String()).Result()
	if err != nil {
		return 0
	}

	e.Attempt = int32(attempt)

	ps.SetExpiry(e)
	// TODO: Do we really care for failure?
	return e.Attempt
}

func (ps *RedisAttemptSession) SetExpiry(e *AttemptSession) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()
	err := ps.client.Expire(ctx, e.String(), 2*time.Minute).Err()
	return err
}
