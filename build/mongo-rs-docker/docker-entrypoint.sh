#!/bin/bash
set -e

REPLICA_SET_NAME=${REPLICA_SET_NAME:=rs0}
USERNAME=${USERNAME:=dev}
PASSWORD=${PASSWORD:=dev}


function waitForMongo {
    port=$1
    n=0
    until [ $n -ge 20 ]
    do
        mongo admin --quiet --port $port --eval "db" && break
        n=$[$n+1]
        sleep 2
    done
}

mkdir -p /data/db1
mkdir -p /data/db2
mkdir -p /data/db3

echo "STARTING CLUSTER"

mongod --port 27003 --dbpath /data/db3 --replSet $REPLICA_SET_NAME --bind_ip=::,0.0.0.0 &
DB3_PID=$!
mongod --port 27002 --dbpath /data/db2 --replSet $REPLICA_SET_NAME --bind_ip=::,0.0.0.0 &
DB2_PID=$!
mongod --port 27001 --dbpath /data/db1 --replSet $REPLICA_SET_NAME --bind_ip=::,0.0.0.0 &
DB1_PID=$!

waitForMongo 27001
waitForMongo 27002
waitForMongo 27003

echo "CONFIGURING REPLICA SET"
# Support for none linux machines
# This condition will take the host ip (if it was configured) from the os environment variable
[[ $HOST_IP ]] && MEMBER_HOST_IP=$HOST_IP || MEMBER_HOST_IP="172.38.0.27"
CONFIG="{ _id: '$REPLICA_SET_NAME', members: [{_id: 0, host: '$MEMBER_HOST_IP:27001' }, { _id: 1, host: '$MEMBER_HOST_IP:27002' }, { _id: 2, host: '$MEMBER_HOST_IP:27003' } ]}"
mongo admin --port 27001 --eval "db.runCommand({ replSetInitiate: $CONFIG })"

waitForMongo 27002
waitForMongo 27003

mongo admin --port 27001 --eval "db.runCommand({ setParameter: 1, quiet: 1 })"
mongo admin --port 27002 --eval "db.runCommand({ setParameter: 1, quiet: 1 })"
mongo admin --port 27003 --eval "db.runCommand({ setParameter: 1, quiet: 1 })"

echo "REPLICA SET ONLINE"

trap 'echo "KILLING"; kill $DB1_PID $DB2_PID $DB3_PID; wait $DB1_PID; wait $DB2_PID; wait $DB3_PID' SIGINT SIGTERM EXIT

wait $DB1_PID
wait $DB2_PID
wait $DB3_PID
